package com.promedia.pruebas.anchoColumnasJTables;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

public class PruebaJTable extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private JScrollPane jScrollPane = null;
	private JTable jTable = null;
	private Controlador controlador = null;

	/**
	 * This is the default constructor
	 */
	public PruebaJTable() {
		super();
		this.controlador = new Controlador(this);
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(800, 600);
		this.setContentPane(getJContentPane());
		this.setTitle("JTable");
		this.addWindowStateListener(new AccionVentana(this));
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJScrollPane(), BorderLayout.CENTER);
		}
		return jContentPane;
	}
	
	/**
	 * This method initializes jScrollPane	
	 * 	
	 * @return javax.swing.JScrollPane	
	 */
	private JScrollPane getJScrollPane() {
		if (jScrollPane == null) {
			jScrollPane = new JScrollPane();
			jScrollPane.setViewportView(getJTable());
		}
		return jScrollPane;
	}

	 private JTable getJTable() {
		if (jTable == null) {
			jTable = new JTable(controlador);
		}
		return jTable;
	}
	 
	protected void setAnchoColumnas(){
		int pantalla = this.getJTable().getParent().getWidth();
		System.out.println(pantalla);
		for(int i = 0; i < this.getJTable().getColumnCount(); i++){
			switch (i) {
			case 0:	
				this.getJTable().getColumnModel().getColumn(i).setPreferredWidth((pantalla * 10) / 100);
				break;
			case 1:	
				this.getJTable().getColumnModel().getColumn(i).setPreferredWidth((pantalla * 50) / 100);
				break;
			case 2:	
				this.getJTable().getColumnModel().getColumn(i).setPreferredWidth((pantalla * 20) / 100);
				break;
			case 3:	
				this.getJTable().getColumnModel().getColumn(i).setPreferredWidth((pantalla * 10) / 100);
				break;
			case 4:	
				this.getJTable().getColumnModel().getColumn(i).setPreferredWidth((pantalla * 10) / 100);
				break;

			default:
				break;
			}
		}
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				PruebaJTable thisClass = new PruebaJTable();
				thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				thisClass.setLocationRelativeTo(null);
				thisClass.setVisible(true);
				thisClass.setAnchoColumnas();
			}
		});
	}

	
	private class Controlador extends AbstractTableModel{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 6298306629526043574L;
		@SuppressWarnings("unused")
		private PruebaJTable pruebaJTable = null;
		private String[] nombreColumna = {"Prueba 1","Prueba 2","Prueba 3","Prueba 4","Prueba 5"};
		private String[][] contenidoTabla = null;
		
		public Controlador(PruebaJTable pruebaJTable){
			super();
			this.pruebaJTable = pruebaJTable;
			this.contenidoTabla = new String[5][5];
			for(int i = 0;i<5;i++){
				for(int j = 0;j<5;j++){
					contenidoTabla[i][j] = i+""+j;
				}
			}			
		}

		@Override
		public int getRowCount() {
			// TODO Auto-generated method stub
			return 5;
		}

		@Override
		public int getColumnCount() {
			// TODO Auto-generated method stub
			return this.nombreColumna.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			return contenidoTabla[rowIndex][columnIndex];
		}

		@Override
		public String getColumnName(int column) {
			// TODO Auto-generated method stub
			return nombreColumna[column];
		}
	}
	
	private class AccionVentana implements WindowStateListener{
		
		private PruebaJTable pruebaJTable = null;
		
		public AccionVentana(PruebaJTable pruebaJTable){
			this.pruebaJTable = pruebaJTable;
		}

		@Override
		public void windowStateChanged(WindowEvent e) {
			pruebaJTable.setAnchoColumnas();
			pruebaJTable.repaint();
		}
		
	}

}
