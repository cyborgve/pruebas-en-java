package com.promedia.pruebas.JColorChooser;
import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class PruebaJColorChooser extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel jContentPane = null;
	private PruebaJColorChooser colorChooser = null;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				PruebaJColorChooser thisClass = new PruebaJColorChooser();
				thisClass.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				thisClass.setVisible(true);
				//thisClass.setUndecorated(true);
				thisClass.setLocationRelativeTo(null);
			}
		});
	}

	/**
	 * This is the default constructor
	 */
	public PruebaJColorChooser() {
		super();
		initialize();
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	private void initialize() {
		this.setSize(432, 300);
		this.setContentPane(getJContentPane());
		this.setTitle("JFrame");
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if (jContentPane == null) {
			jContentPane = new JPanel();
			jContentPane.setLayout(new BorderLayout());
			jContentPane.add(getJColorChooser());
		}
		return jContentPane;
	}
	
	private PruebaJColorChooser getJColorChooser(){
		if(colorChooser == null){
			colorChooser = new PruebaJColorChooser();
		}
		return colorChooser;
	}

}
